using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToastManager : MonoBehaviour
{
    public static ToastManager Instance { get; private set; }
    public MeshRenderer mesh;
    public Color successColor;
    public Color loadingColor;
    public Color failColor;
    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }

        transform.position = Vector3.zero;
    }

    private string _toastString = "Sample Text";
    public string ToastMsg { set { _toastString = value; } get { return _toastString; } }

    private void OnMouseDown()
    {

        ShowToastMessage();
    }
    public void Success()
    {
        mesh.material.color = successColor;
    }
    public void Fail()
    {
        mesh.material.color = failColor;
    }
    public void Loading()
    {
        mesh.material.color = loadingColor;
    }
    public void ShowToastMessage()
    {
        try
        {

#if UNITY_EDITOR
            Debug.Log($"Toast : {_toastString}");
#elif UNITY_ANDROID
            AndroidToast();
#endif
        }
        catch (System.Exception e)
        {
            Debug.LogError("Error fond in Show Toast " + e.Message);
        }
    }

    private void AndroidToast()
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, _toastString, 0);
                toastObject.Call("show");
            }));
        }
    }
}
